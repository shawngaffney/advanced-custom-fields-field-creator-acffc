* ACFFC -  Advanced Custom Fields Field Creator *
*************************************************
Version: 1.0
Contributors: Jamie Telin (jamie.telin@gmail.com)
Licens: Creative Commons Attribution-ShareAlike 3.0 Unported (http://creativecommons.org/licenses/by-sa/3.0/)

Description
-------------------- 
ACFFC is a "api" layer to ACF for creating new field groups and fields though a theme.
It gives the developer a more structured OOP approach to creating fields.
It auto completes any missing variables, allowing developer to quickly add new fields for their themes.
It also automaticly generates ID and saves them in options for cacheing.

Usage example
--------------------

	$ACFFC->add_field_group( 'product_page',  'Product page');
	$ACFFC->add_text('product_page', array(
		'label'  => 'Title' ,
		'name'  => 'title' ,
		'formatting'  => 'none' ,
		));
	$ACFFC->add_textarea('product_page', array(
		'label'  => 'Short Content' ,
		'name'  => 'short_content' ,
		'instructions'  => 'Enter a short version of content.' ,
		));
	$ACFFC->add_wysiwyg('product_page', array(
		'label'  => 'Content' ,
		'name'  => 'content' ,
		'formatting'  => 'none' ,
		));
	$ACFFC->add_image('product_page', array(
		'label'  => 'Image' ,
		'name'  => 'image' ,
		));
	$ACFFC->add_rule('product_page', array(
		'param'  => 'page_template' ,
		'operator'  => '==' ,
		'value'  => 'tpl-productpage.php' ,
		));
	$ACFFC->add_hide_on_screen('product_page', array(
		'the_content', 'custom_fields', 'featured_image'
	));